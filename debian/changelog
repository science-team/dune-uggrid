dune-uggrid (2.10.0-1) unstable; urgency=medium

  * New upstream version 2.10.0
  * Added debian/watch file
  * d/copyright: Use Files-Excluded to exclude files
  * d/rules: Use chrpath to strip RPATH/RUNPATH from installed libs
  * d/control: Bumped Standards version to 4.7.0 (no changes)
  * d/control: Depend on pkgconf.
  * d/control: Depend on DUNE 2.10

 -- Markus Blatt <markus@dr-blatt.de>  Thu, 14 Nov 2024 20:50:33 +0100

dune-uggrid (2.10~pre20240905-2) experimental; urgency=medium

  * d/rules: Use chrpath to strip RPATH/RUNPATH from installed libs

 -- Markus Blatt <markus@dr-blatt.de>  Mon, 21 Oct 2024 23:47:59 +0200

dune-uggrid (2.10~pre20240905-1) experimental; urgency=medium

  * New upstream release (2.10~pre20240905).
  * d/control: Bumped Standards version to 4.7.0 (no changes)
  * d/control: Depend on pkgconf.
  * d/control: Depend on DUNE 2.10

 -- Markus Blatt <markus@dr-blatt.de>  Thu, 17 Oct 2024 20:37:41 +0200

dune-uggrid (2.9.0-2) unstable; urgency=medium

  * d/control: Added Markus Blatt as uploader (with consent of Ansgar)
  * Upload to unstable

 -- Markus Blatt <markus@dr-blatt.de>  Thu, 12 Jan 2023 18:38:59 +0100

dune-uggrid (2.9.0-1) experimental; urgency=medium

  [ Markus Blatt ]
  * d/upstream: Added metadata file
  * New upstream release (2.9.0).

  [ Ansgar ]
  * d/control: set Standards-Version to 4.6.2 (no changes).

 -- Ansgar <ansgar@debian.org>  Sun, 08 Jan 2023 10:30:27 +0100

dune-uggrid (2.8.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Patrick Jaap <patrick.jaap@tu-dresden.de>  Thu, 21 Oct 2021 18:45:48 +0200

dune-uggrid (2.8.0-1) experimental; urgency=medium

  * New upstream release.

 -- Patrick Jaap <patrick.jaap@tu-dresden.de>  Wed, 15 Sep 2021 11:28:40 +0200

dune-uggrid (2.8.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * debian/rules: Explicitly set buildsystem to CMake

 -- Patrick Jaap <patrick.jaap@tu-dresden.de>  Mon, 23 Aug 2021 07:24:38 -0400

dune-uggrid (2.7.1-2) unstable; urgency=medium

  * Upload to unstable.

 -- Ansgar <ansgar@debian.org>  Mon, 11 Jan 2021 22:32:52 +0100

dune-uggrid (2.7.1-1) experimental; urgency=medium

  * New upstream release

 -- Patrick Jaap <patrick.jaap@tu-dresden.de>  Thu, 07 Jan 2021 10:15:21 +0100

dune-uggrid (2.7.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Ansgar <ansgar@debian.org>  Wed, 15 Jul 2020 12:36:31 +0200

dune-uggrid (2.7.0-1) experimental; urgency=medium

  * New upstream release.
  * Use debhelper compat level 13.
  * Bumped Standards-Version to 4.5.0 (no changes).

 -- Ansgar <ansgar@debian.org>  Mon, 25 May 2020 16:15:34 +0200

dune-uggrid (2.6.0-1) unstable; urgency=medium

  * New upstream release.

 -- Ansgar Burchardt <ansgar@debian.org>  Tue, 03 Apr 2018 22:42:23 +0200

dune-uggrid (2.6~20180108-1) experimental; urgency=medium

  * New upstream snapshot (commit: b30bd26b596a7298f4c5d9fcdde4209f630483d4)
  * d/control: update Vcs-* fields for move to salsa.debian.org
  * Bumped Standards-Version to 4.1.3.

 -- Ansgar Burchardt <ansgar@debian.org>  Mon, 08 Jan 2018 11:41:48 +0100

dune-uggrid (2.6~20171120-1) experimental; urgency=medium

  * New upstream snapshot (commit: 233efec13770d21ed952438ae78c6e80fa9fff69)
  * Bumped Standards-Version to 4.1.1 (no changes).

 -- Ansgar Burchardt <ansgar@debian.org>  Mon, 20 Nov 2017 22:41:31 +0100

dune-uggrid (2.5.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/copyright: Upgrade copyright format URL to https://.
  * Bumped Standards-Version to 4.0.0.

 -- Ansgar Burchardt <ansgar@debian.org>  Tue, 18 Jul 2017 12:04:21 +0200

dune-uggrid (2.5.1~rc1-1) experimental; urgency=medium

  * New upstream release candidate.
  * No longer build manual -dbg package. Use the automatically generated
    -dbgsym package instead.

 -- Ansgar Burchardt <ansgar@debian.org>  Sat, 17 Jun 2017 16:15:42 +0200

dune-uggrid (2.5.0-1) unstable; urgency=medium

  * New upstream release.

 -- Ansgar Burchardt <ansgar@debian.org>  Sun, 18 Dec 2016 16:09:47 +0100

dune-uggrid (2.5.0~rc2-1) unstable; urgency=medium

  * Upload to unstable.
  * New upstream release candidate.
  * Override lintian warnings:
    - package-name-doesnt-match-sonames
    - non-dev-pkg-with-shlib-symlink
    - no-symbols-control-file

 -- Ansgar Burchardt <ansgar@debian.org>  Tue, 06 Dec 2016 00:56:05 +0100

dune-uggrid (2.5.0~rc1-2) experimental; urgency=medium

  * Install /usr/share/dune and /usr/share/dune-uggrid.

 -- Ansgar Burchardt <ansgar@debian.org>  Thu, 01 Dec 2016 18:18:48 +0100

dune-uggrid (2.5.0~rc1-1) experimental; urgency=medium

  * New upstream release candidate.
  * Package the dune-uggrid module: We are only interested in the UG grid
    manager for DUNE. The old UG library will no longer be maintained in
    the future.
  * Bumped Standards-Version to 3.9.8 (no changes).

 -- Ansgar Burchardt <ansgar@debian.org>  Mon, 21 Nov 2016 20:53:47 +0100

ug (3.12.1-2) unstable; urgency=medium

  * Upload to unstable.

 -- Ansgar Burchardt <ansgar@debian.org>  Sun, 13 Sep 2015 13:21:24 +0200

ug (3.12.1-1) experimental; urgency=medium

  * New upstream release.
  * Change shared library package name to libug3.12.1.
  * Mention that UG has been built with support for DUNE in the
    package description.
  * Bump Standards-Version to 3.9.6 (no changes).

 -- Ansgar Burchardt <ansgar@debian.org>  Mon, 11 May 2015 21:05:04 +0200

ug (3.11.0-1) unstable; urgency=medium

  * New upstream release.
  * Change shared library package name to libug3.11.0.

 -- Ansgar Burchardt <ansgar@debian.org>  Sun, 15 Jun 2014 11:12:53 +0200

ug (3.10.0-1) unstable; urgency=low

  * Initial release. (Closes: #742179)

 -- Ansgar Burchardt <ansgar@debian.org>  Tue, 08 Apr 2014 02:07:52 +0200
