Source: dune-uggrid
Section: libs
Priority: optional
Standards-Version: 4.7.0
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Ansgar <ansgar@debian.org>, Markus Blatt <markus@dr-blatt.de>
Vcs-Browser: https://salsa.debian.org/science-team/dune-uggrid
Vcs-Git: https://salsa.debian.org/science-team/dune-uggrid.git
Homepage: https://gitlab.dune-project.org/staging/dune-uggrid/
Build-Depends: debhelper-compat (= 13),
 cmake, gfortran, mpi-default-bin, mpi-default-dev, pkgconf, python3,
 libdune-common-dev (>= 2.10~), chrpath
Rules-Requires-Root: no

Package: libdune-uggrid-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends},
 libdune-common-dev (>= 2.10~)
Provides: ${dune:shared-library}
Conflicts: libug-dev
Replaces: libug-dev
Description: software framework for finite element methods (development files)
 UG is a flexible software tool for the numerical solution of partial
 differential equations on unstructured meshes, with a focus on
 multigrid methods. It has a very powerful grid manager, which supports
 two- and three-dimensional grids with mixed element types. The grids
 can be adaptively refinement using either classic
 red/green-refinement, or pure red refinement with hanging nodes. All
 this is fully parallelized and can run on large distributed machines.
 .
 The version in this package has been built with the necessary flags for use
 with the Dune software system.
 .
 This package contains the development files.
